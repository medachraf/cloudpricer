import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersOrganizationRoutingModule } from './users-organization-routing.module';
import { UsersOrganizationComponent } from './users-organization/users-organization.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UsersOrganizationComponent
  ],
  imports: [
    CommonModule,
    UsersOrganizationRoutingModule,
    FormsModule
  ]
})
export class UsersOrganizationModule { }
