import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/views/services/data.service';
import { Userorganization } from '../../userorganization';

@Component({
  selector: 'app-users-organization',
  templateUrl: './users-organization.component.html',
  styleUrls: ['./users-organization.component.css']
})
export class UsersOrganizationComponent implements OnInit {

  allUserOrg: Userorganization[] = [];
  //datatArray:any=[]
  constructor(private ds:DataService) {
  // this.ds.getAllUsersOrg().subscribe(data=>this.datatArray=data)
    this.ds.getAllUsersOrg().subscribe((data)=>{
      this.allUserOrg=data;
    })
}

  ngOnInit(): void {
  }

 // delete(id:any, i:number){
  //  this.ds.deleteUserOrg(id).subscribe(response=>{
  //    this.datatArray.splice(i,1)
  //  })
 // }

 deleteItem(id:number){
  this.ds.deleteUserOrg(id).subscribe({
    next:(data)=>{
      this.allUserOrg = this.allUserOrg.filter(_ => _.id != id)
    },
    error: (er) =>{
      console.log(er)
    }
  })
}

}
