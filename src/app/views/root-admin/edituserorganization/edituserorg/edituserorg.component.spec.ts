import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdituserorgComponent } from './edituserorg.component';

describe('EdituserorgComponent', () => {
  let component: EdituserorgComponent;
  let fixture: ComponentFixture<EdituserorgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdituserorgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdituserorgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
