import { Component, OnInit } from '@angular/core';
import { Userorganization } from '../../userorganization';
import { DataService } from 'src/app/views/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edituserorg',
  templateUrl: './edituserorg.component.html',
  styleUrls: ['./edituserorg.component.css']
})
export class EdituserorgComponent implements OnInit {

  constructor(private ds: DataService, private router: Router, private route: ActivatedRoute) { }

  formData: Userorganization = {
    id: 0,
    name: '',
    email: '',
    password: '',
    organization_name:'',
    amout_user:0
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((param)=>{
      let id = Number(param.get('id'))
      this.getById(id)
    })
  }

  getById(id:number){
    this.ds.editUserOrg(id).subscribe((data)=>{
      this.formData = data;
    })
  }

  update(){
    this.ds.updateUserOrg(this.formData).subscribe({
      next: (data)=>{
        this.router.navigate(["/root-admin/users-organization"])
      },
      error: (err)=>{
        console.log(err)
      }
    })
  }

}
