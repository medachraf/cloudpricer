import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EdituserorganizationRoutingModule } from './edituserorganization-routing.module';
import { EdituserorgComponent } from './edituserorg/edituserorg.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EdituserorgComponent
  ],
  imports: [
    CommonModule,
    EdituserorganizationRoutingModule,
    FormsModule
  ]
})
export class EdituserorganizationModule { }
