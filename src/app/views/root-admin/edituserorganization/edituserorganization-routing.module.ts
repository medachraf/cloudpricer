import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EdituserorgComponent } from './edituserorg/edituserorg.component';

const routes: Routes = [
  {path: '', component: EdituserorgComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EdituserorganizationRoutingModule { }
