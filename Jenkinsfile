pipeline {
    agent any
    
    environment {
        DOCKERHUB_CREDENTIALS = credentials('dockerhub')
        DOCKER_IMAGE = 'medach20/cloudpricer'
        DOCKER_TAG = 'latest'
    }

    stages {
        stage('Checkout') {
            steps {
                git url: 'https://gitlab.com/medachraf/cloudpricer.git', branch: 'main'
            }
        }


        stage('Install Node.js and npm with nvm') {
            steps {
                sh '''
                curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
                export NVM_DIR="$HOME/.nvm"
                . "$NVM_DIR/nvm.sh"
                nvm install 18
                node -v
                npm -v
                '''
            }
        }

        stage('SonarQube Analysis') {
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh '''
                    export NVM_DIR="$HOME/.nvm"
                    . "$NVM_DIR/nvm.sh"
                    npm install sonarqube-scanner
                    ./node_modules/.bin/sonar-scanner \
                      -Dsonar.projectKey=cloudpricer \
                      -Dsonar.sources=. \
                      -Dsonar.host.url=http://192.168.101.139:9000 \
                      -Dsonar.login=b085dc9f75698e00e17451b6a7db571eec8836de
                    '''
                }
            }
        }

        stage("Quality gate") {
            steps {
                waitForQualityGate abortPipeline: true
            }
        }
        
        stage('Build Docker Image') {
            steps {
                script {
                    docker.build("${DOCKER_IMAGE}:${DOCKER_TAG}")
                }
            }
        }

        stage('Login') {
            steps {
              script { 
                sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
                } 
            }
        }

        stage('Push Docker Image') {
            steps {
                script {
                        docker.image("${DOCKER_IMAGE}:${DOCKER_TAG}").push()
                }
            }
        }

        stage('Security Scan with Trivy') {
            steps {
                // Scanner l'image Docker avec Trivy
                script {
                    sh 'trivy image --severity HIGH,CRITICAL ${DOCKER_IMAGE}:${DOCKER_TAG}'
                }
            }
        }

        stage('Deploying app to kubernetes'){
            steps{
                script{
                    def configFiles = ["mysql-pvc.yml", "mysql-deployment.yml", "mysql-service.yml", "deployment.yml"].join(',')
                    kubernetesDeploy(configs: configFiles, kubeconfigId: "kubernetes")
                }
            }
        }

        stage('Kube-bench Security Scan') {
            steps {
                script {
                    // Exécutez Kube-bench sur le cluster Kubernetes
                    sh '''
                    docker run --rm -v $(pwd)/kube-bench:/host aquasec/kube-bench:latest install
                    docker run --rm --network host -v $(pwd)/kube-bench:/host aquasec/kube-bench:latest --config-dir /host/cfg --config /host/cfg/config.yaml
                    '''
                }
            }
        }

        stage('Sending email'){
            steps{
                mail bcc: '', body: '''
                Pipeline successfully executed .
                Keep Up The Good Work''', cc: '', from: '', replyTo: '', subject: 'DevSecOps Pipeline', to: 'mohamedachrafhamrouni@gmail.com'
            }
        }
    }
}
